/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.registry;

import it.polimi.hegira.registry.SchemaRegistryDAO.Schema;
import it.polimi.hegira.registry.utils.PropertiesManager;

public class TestClass {

	public static void main(String[] args) {


		SchemaRegistry registry = SchemaRegistry.getInstance();
		System.out.println(registry.getCellsType("TABELLA_1", "quantity"));
		
		Schema schema = registry.takeSchema().getSchema();
		registry.switchToSecundarySchema();
		try{
			String schemaJSON = "{\"type\":\"record\",\"name\":\"ESEMPIO_SCHEMA\",\"fields\":[{\"name\":\"TABELLA_1\",\"type\":{\"items\":{\"name\":\"ENTRY_TABELLA_1\",\"type\":\"record\",\"fields\":[{\"name\":\"quantity\",\"type\":\"int\"},{\"name\":\"total\",\"type\":\"float\"},{\"name\":\"product_detail\",\"type\":\"string\"}]},\"type\":\"array\"}},{\"name\":\"TABELLA_2\",\"type\":{\"items\":{\"name\":\"ENTRY_TABELLA_2\",\"type\":\"record\",\"fields\":[{\"name\":\"quantity\",\"type\":\"int\"},{\"name\":\"total\",\"type\":\"float\"},{\"name\":\"product_detail\",\"type\":\"string\"}]},\"type\":\"array\"}}]}";
			System.out.println(registry.setSchemaFromJson(schemaJSON));
		}
		catch(Exception e){
			System.err.println(e.getMessage());
		}
	}

}
