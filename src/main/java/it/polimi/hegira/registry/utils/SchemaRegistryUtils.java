/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.registry.utils;

public class SchemaRegistryUtils {
	public static String mapToJavaType(String schemaRegistryType){
		switch(schemaRegistryType.toLowerCase()){
			case "string":		
				return "String";
			case "long":
				return "Long";
			case "bytes":
				return "byte[]";
			case "boolean":
				return "Boolean";	
			case "double":
				return "Double";		
			case "float":
				return "Float";
			case "int":	
				return "Integer";
			default: 
				return "byte[]";
		}
	}
	
	public static Object convertToJavaType(byte[] actualValue, String schemaRegistryType, String serializer){
		Object unserializedValue = null;
		
		try{
			switch(serializer.toLowerCase()){
				case "hbaseshellserializer":
					unserializedValue = HBaseShellSerializer.deserialize(actualValue);
					break;
				case "defaultserializer":
					unserializedValue = DefaultSerializer.deserialize(actualValue);
					break;
				default:
					unserializedValue = DefaultSerializer.deserialize(actualValue);
					break;
			}
		}
		catch( Exception e){
			e.printStackTrace();
			return null;
		}
		
		switch(schemaRegistryType.toLowerCase()){
			case "string":
				return (String)unserializedValue;
			case "long":
				return (Long)unserializedValue;
			case "byte[]":
				return actualValue;
			case "boolean":
				return (Boolean)unserializedValue;	
			case "double":
				return (double)unserializedValue;		
			case "float":
				return (float)unserializedValue;
			case "integer":
				return (int)unserializedValue;
			default: 
				return actualValue;
		}	
	}
}
