/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.registry.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;


/**
 * Utility class to retrieve data from properties files.
 * @author Marco Scavuzzo
 *
 */
public class PropertiesManager {
	//private transient static Logger log = LoggerFactory.getLogger(PropertiesManager.class);
	private static String defaultPath = System.getProperty("user.home")+File.separator+
			"hegira"+File.separator+
			"registry"+File.separator;
	
	/**
	 * Gets the value of a given property stored inside the given file.
	 * @param fileName the file name in the default path (user.home)
	 * @param property	The name of the property to retrieve.
	 * @return	The value associated to the given property name.
	 */
	public static String getProperty(String fileName, String property){
		Properties props = new Properties();
		InputStream isr = null;
		try {
			isr = new FileInputStream(createFileIfNotExists(defaultPath, fileName));	
			props.load(isr);
			return props.getProperty(property);
		} catch (IOException e) {
			
		} catch (Exception e) {
			
		} finally {
			try {
				if(isr!=null)
					isr.close();
			} catch (IOException e) {}
			props=null;
		}
		return null;
	}
	
	private static File createFileIfNotExists(String path, String fileName) 
			throws NullPointerException, IOException{
		if(fileName == null)
			throw new NullPointerException("fileName cannot be null");
		if(path==null)
			path=defaultPath;
		
		File file = new File(path+fileName);
		if (!file.getParentFile().exists())
		    file.getParentFile().mkdirs();
		if (!file.exists())
		    file.createNewFile();
		return file;
	}	
}
