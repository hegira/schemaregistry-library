/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.registry;

import java.util.List;

import com.google.gson.Gson;

public class SchemaRegistryDAO {
	 private String subject;
     private String version;
     private String id;
     private String schema;
     
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Schema getSchema() {
		Gson gson = new Gson();
		return (Schema) gson.fromJson(schema, Schema.class);
	}
	public void setSchema(String schema) {
		this.schema = schema;
	}
	
	public class Schema{
		private String type;
		private String name;
		private List<SchemaTable> fields;
		
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public List<SchemaTable> getFields() {
			return fields;
		}
		public void setFields(List<SchemaTable> fields) {
			this.fields = fields;
		}		
		
		public int tablesCount(){
			return this.fields.size();
		}
		
		public String getColumnType( String tableKey, String columnKey ){
			for( SchemaTable table : fields ){
				if( table.getName().equalsIgnoreCase(tableKey) ){
					for( TableField column : table.getType().getItems().getFields()){
						if(column.getName().equalsIgnoreCase(columnKey))
							return column.getType();
					}
					
				}
			}
			return null;
		}
		
		public String getColumnSerializer( String tableKey, String columnKey ){
			for( SchemaTable table : fields ){
				if( table.getName().equalsIgnoreCase(tableKey) ){
					for( TableField column : table.getType().getItems().getFields()){
						if(column.getName().equalsIgnoreCase(columnKey))
							return column.getSerializer();
					}
					
				}
			}
			return null;
		}
		
		public List<String> getPrimaryIndex( String tableKey ){
			for( SchemaTable table : fields ){
				if( table.getName().equalsIgnoreCase(tableKey) ){
					for( TableIndex index : table.getIndexes()){
						if(index.getType().equalsIgnoreCase("primary"))
							return index.getColumns();
					}
					
				}
			}
			return null;
		}
		
		public boolean isIndexable( String tableKey, String columnKey ){
			for( SchemaTable table : fields ){
				if( table.getName().equalsIgnoreCase(tableKey) ){
					for( TableIndex index : table.getIndexes()){
						for( String column : index.getColumns() ){
							if( column.equalsIgnoreCase(columnKey) ) 
								return true;
						}
					}
					
				}
			}
			return false;
		}
		
		public List<TableIndex> getIndexes( String tableKey ){
			for( SchemaTable table : fields ){
				if( table.getName().equalsIgnoreCase(tableKey) ){
					return table.getIndexes();
					
				}
			}
			return null;
		}
	}
	
	public class SchemaTable{
		private String name;
		private TableType type;
		private List<TableIndex> indexes;
		
		public List<TableIndex> getIndexes() {
			return indexes;
		}
		public void setIndexes(List<TableIndex> indexes) {
			this.indexes = indexes;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public TableType getType() {
			return type;
		}
		public void setType(TableType type) {
			this.type = type;
		}
		
		public int columnsCount(){
			return this.type.getItems().getFields().size();
		}
		
	}
	
	public class TableIndex{
		private String name;
		private  String type;
		private List<String> columns;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public List<String> getColumns() {
			return columns;
		}
		public void setColumns(List<String> columns) {
			this.columns = columns;
		}
	}
	
	public class TableType{
		private TableItem items;
		private String type;
		
		public TableItem getItems() {
			return items;
		}
		public void setItems(TableItem name) {
			this.items = name;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		
	}
	
	public class TableItem{
		private String name;
		private String type;
		private List<TableField> fields;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public List<TableField> getFields() {
			return fields;
		}
		public void setFields(List<TableField> fields) {
			this.fields = fields;
		}
		
	}
	
	public class TableField{
		private String name;
		private String type;
		private String serializer;
		
		public String getSerializer() {
			return serializer;
		}
		public void setSerializer(String serializer) {
			this.serializer = serializer;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		
	}
		
		

}
