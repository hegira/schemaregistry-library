/**
 * Copyright ${year} Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.registry;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;

import it.polimi.hegira.registry.SchemaRegistryDAO.Schema;
import it.polimi.hegira.registry.SchemaRegistryDAO.SchemaTable;
import it.polimi.hegira.registry.SchemaRegistryDAO.TableIndex;
import it.polimi.hegira.registry.exceptions.InvalidInputException;
import it.polimi.hegira.registry.utils.PropertiesManager;


public class SchemaRegistry {
	
	private String baseUrl;
	private String schemaName;
	private SchemaRegistryDAO schema;
	
	private static class SchemaRegistryHolder {
		private static final SchemaRegistry instance = new SchemaRegistry();
	}
	
	private SchemaRegistry() {
		baseUrl = PropertiesManager.getProperty("schemaRegistry.properties", "registryAddress");
		schemaName = PropertiesManager.getProperty("schemaRegistry.properties", "primarySchemaName");
		schema = this.getSchema();
	}
	
	public static SchemaRegistry getInstance(){
		return SchemaRegistryHolder.instance;
	}
	
	public SchemaRegistryDAO takeSchema(){
		return this.schema;
	}
	
	public void switchToPrimarySchema(){
		PropertiesManager.getProperty("schemaRegistry.properties", "primarySchemaName");
	}
	
	public void switchToSecundarySchema(){
		PropertiesManager.getProperty("schemaRegistry.properties", "secundarySchemaName");	
	}
	
	public void refreshSchema(){
		schema = this.getSchema();
	}
	
	public String getCellsType( String tableKey, String columnKey ){
		return schema.getSchema().getColumnType(tableKey, columnKey);
	}
	
	public String getCellsSerializer( String tableKey, String columnKey ){
		return schema.getSchema().getColumnSerializer(tableKey, columnKey);
	}
	
	public boolean isIndexable( String tableKey, String columnKey ){
		return schema.getSchema().isIndexable(tableKey, columnKey);
	}
	
	public List<String> getPrimaryIndex( String tableKey ){
		return schema.getSchema().getPrimaryIndex(tableKey);
	}
	
	public List<TableIndex> getIndexes( String tableKey ){
		return schema.getSchema().getIndexes(tableKey);
	}
	
	private SchemaRegistryDAO getSchema(){
		
		String schemaUrl = baseUrl + "/subjects/" + schemaName + "/versions/latest";
		
		try {
			HttpClient httpClient = new DefaultHttpClient();
            HttpGet request = new HttpGet(schemaUrl);
            request.addHeader("content-type", "application/json");
            HttpResponse result = httpClient.execute(request);
            String json = EntityUtils.toString(result.getEntity(), "UTF-8");

            Gson gson = new Gson();
            SchemaRegistryDAO response = (SchemaRegistryDAO) gson.fromJson(json, SchemaRegistryDAO.class);
            
            return response;
        } catch (IOException ex) {
        	System.err.println(ex.getMessage());
        }
		
		return null;
	}
	
	public String setSchemaFromJson( String JSONSchema ) throws InvalidInputException{
		Gson gson = new Gson();
		Schema object = null;
		object = (Schema) gson.fromJson(JSONSchema, Schema.class);
		boolean isObjectValid = (object.tablesCount() > 0)? true : false;

		if( isObjectValid ){
			for( SchemaTable table : object.getFields()){
				if( table.columnsCount() < 1 )
					isObjectValid = false;
			}
			
			if( isObjectValid ){
				try{
					String schemaUrl = baseUrl + "/subjects/" + schemaName + "/versions";
					String requestBody = "{\"schema\" : \"" + JSONSchema.replaceAll("\"", "\\\\\"") + "\"}";
					
					HttpClient httpClient = new DefaultHttpClient();
		            HttpPost request = new HttpPost(schemaUrl);
		            request.addHeader("content-type", "application/vnd.schemaregistry.v1+json");        
		            request.setEntity(new StringEntity(requestBody));
		            HttpResponse result = httpClient.execute(request);
		            String response = EntityUtils.toString(result.getEntity(), "UTF-8");
            
		            return response;
		        } catch (Exception e) {
		        	System.err.println(e.getMessage());
		        }
			}
			else{
				throw new InvalidInputException("The input objects does not match the required structure. At least one table has no columns.");
			}
		}
		else{
			throw new InvalidInputException("The input objects does not match the required structure. At least one table has no columns.");
		}
		
		return null;
	}
	
	public String setSchemaFromObject( Schema object ) throws InvalidInputException{
		Gson gson = new Gson();
		String JSONSchema = "";
		boolean isObjectValid = (object.tablesCount() > 0)? true : false;
		
		if( isObjectValid ){
			for( SchemaTable table : object.getFields()){
				if( table.columnsCount() < 1 )
					isObjectValid = false;
			}
			
			if( isObjectValid ){
				try{
					JSONSchema = (String) gson.toJson(object);
					String schemaUrl = baseUrl + "/subjects/" + schemaName + "/versions";
					String requestBody = "{\"schema\" : \"" + JSONSchema.replaceAll("\"", "\\\\\"") + "\"}";
					
					System.out.println(requestBody);
					
					HttpClient httpClient = new DefaultHttpClient();
		            HttpPost request = new HttpPost(schemaUrl);
		            request.addHeader("content-type", "application/vnd.schemaregistry.v1+json");        
		            request.setEntity(new StringEntity(requestBody));
		            HttpResponse result = httpClient.execute(request);
		            String response = EntityUtils.toString(result.getEntity(), "UTF-8");
		            
		            return response;
				}
				catch( Exception ex ) {
					System.err.println(ex.getMessage());
				}
			}
			else{
				throw new InvalidInputException("The input objects does not match the required structure. At least one table has no columns.");
			}
		}
		else{
			throw new InvalidInputException("The input objects does not match the required structure. No tables are defined.");
		}
		
		return null;
	}
	
}
